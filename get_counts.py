import requests
import json
from tqdm import tqdm
from multiprocessing import Pool, freeze_support
import os

cursor = ""

table = []

def get_count(d):
    id, name, city = d
    x = requests.get(
        f'https://supportculture.migros.ch/api/v1/frontend/organisation-public/{id}/counts',
        headers={"User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64)"}
    )
    return id, name, city, json.loads(x.text)["voucherCount"]

if __name__ == '__main__':
    freeze_support()

    with open("list.csv", "r", encoding="utf-8") as f_in, open("data.csv", "w", encoding="utf-8") as f_out, Pool(60) as pool:
        data = [l.strip("\n").split(";") for l in f_in.readlines()[1:]]
        f_out.write("id;name;city;count\n")
        for id, name, city, count in tqdm(pool.imap_unordered(get_count, data), total=len(data)):
            f_out.write(f"{id};{name};{city};{count}\n")
