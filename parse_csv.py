import numpy as np

def get_elem(data, idx):
    for x in data:
        if x[0] == idx:
            return x
    return None
    
with open("data.csv", "r", encoding="utf-8") as f:
    data = [l.split(";") for l in f.readlines()[1:]]
    nb_bons = [int(x[3]) for x in data]
    total_count = sum(nb_bons)

    fanfare = get_elem(data, "a6ab36e8-2aa4-4868-9774-f49364560c59")
    rank = np.where(np.array(sorted(nb_bons, reverse=True)) == int(fanfare[3]))[0][0]

    total_pot = 6e6
    pot_to_share = total_pot - 100 * len(data)

    earned = pot_to_share / total_count * int(fanfare[3]) + 100

    print(f"{fanfare[1]}, {fanfare[2]} a récolté {int(fanfare[3])} bons, ce qui la place {rank}ème sur {len(data)}.\nEstimation du montant: ~{earned:.0f}CHF")