import requests
import json
from tqdm import tqdm

cursor = ""

table = []

pbar = tqdm()

with open("list.csv", "w", encoding="utf-8") as f:
    f.write("id;name;city\n")
    while cursor is not None:
        x = requests.get(
            f'https://supportculture.migros.ch/api/v1/frontend/organisation-search-public/?cursor={cursor}',
            headers={"User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64)"}
        )
        result = json.loads(x.text)
        if cursor == "":
            pbar.total = result["totalCount"]
        for r in result["results"]:
            name = r["name"]
            city = r["city"]
            id = r["publicId"]
            f.write(f"{id};{name};{city}\n")
            pbar.update(1)

        cursor = result["next"]
